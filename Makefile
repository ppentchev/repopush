#!/usr/bin/make -f
#
# Copyright (c) 2021  Peter Pentchev <roam@ringlet.net>
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
# ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
# OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
# HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
# OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
# SUCH DAMAGE.

PROG=		repopush
PROG_SH=	${PROG}.sh
MAN_SCD=	${PROG}.scd
MAN1=		${PROG}.1
MAN1GZ=		${MAN1}.gz

LOCALBASE?=	/usr/local
PREFIX?=	${LOCALBASE}

BINDIR?=	${PREFIX}/bin
MANDIR?=	${PREFIX}/man/man

DIRMODE?=	755

BINOWN?=	root
BINGRP?=	root
BINMODE?=	755

SHAREOWN?=	${BINOWN}
SHAREGRP?=	${BINGRP}
SHAREMODE?=	644

GZIP?=		gzip
GZIP_ARGS?=	-cn9
MKDIR_P?=	mkdir -p -m ${DIRMODE}
INSTALL?=	install

INSTALL_PROGRAM?=	${INSTALL} -o ${BINOWN} -g ${BINGRP} -m ${BINMODE}
INSTALL_DATA?=		${INSTALL} -o ${SHAREOWN} -g ${SHAREGRP} -m ${SHAREMODE}

PYTHON?=	python3
PYTHON_ENV?=	PYTHONPATH='${CURDIR}/python' PYTHONUNBUFFERED=1

all:		${PROG} ${MAN1GZ}

${PROG}:	${PROG_SH}
		${INSTALL} -m 755 -- '${PROG_SH}' '${PROG}'

${MAN1}:	${MAN_SCD}
		scdoc < '${MAN_SCD}' > '${MAN1}' || { rm -f -- '${MAN1}'; false; }

${MAN1GZ}:	${MAN1}
		${GZIP} ${GZIP_ARGS} -- '${MAN1}' > '${MAN1GZ}' || { rm -f -- '${MAN1GZ}'; false; }

clean:
		rm -f -- '${PROG}' '${MAN1GZ}'

test:		all
		${MAKE} test-shellcheck
		${MAKE} test-python

test-shellcheck:
		[ -n "$$SKIP_SHELLCHECK" ] || shellcheck -- '${PROG_SH}'

test-python:
		[ -n "$$SKIP_TOX" ] || ${PYTHON} -m tox

install:	${PROG} ${MAN1}
		[ -d '${DESTDIR}${BINDIR}' ] || ${MKDIR_P} -- '${DESTDIR}${BINDIR}'
		${INSTALL_PROGRAM} -- '${PROG}' '${DESTDIR}${BINDIR}'
		[ -d '${DESTDIR}${MANDIR}1' ] || ${MKDIR_P} -- '${DESTDIR}${MANDIR}1'
		${INSTALL_DATA} -- '${MAN1}' '${DESTDIR}${MANDIR}1'

.PHONY:	all clean install test test-python
