# repopush - safely rsync a package repository to another location

## Description

The *repopush* tool is yet another program that helps with copying
the various package files and repository metadata from one OS package
repository to another, taking care to copy and remove files in such
a way as to never have the remote repository present an inconsistent view.
This is done by first copying the new package files, then copying the new
metadata files, then removing the outdated metadata files, and only then
removing the outdated package files.

The *repopush* tool supports APT and YUM repositories and figures out
which one to handle by analyzing the contents of the local repository.
This is one of the reasons why the latter must indeed be a directory on
a locally-mounted filesystem.

There are two ways to specify the _local_ and _remote_ parameters:
directly on the command line or in a INI-style configuration file.
If the *-s* option is specified, *repopush* will use the *confget* tool to
read the configuration file (either the one specified by the *-f* option or
a default one in the user's home directory), look for the specified section,
and use the "local" and "remote" values from that section.

As noted above, the _local_ parameter should be a path to a directory.
The _remote_ parameter can be in any format that the *rsync* tool
accepts.

## Examples

Test-run a repository update:

        repopush -Nv /var/lib/repos/infra www:/var/www/repo/infra

Perform the actual update, a bit quieter:

        repopush /var/lib/repos/infra www:/var/www/repo/infra

The same, but with the "to-www-infra" section specified in
the _$HOME_*/.config/repopush.conf* file:

        repopush -s to-www-infra

To achieve the above result, the *repopush.conf* file should contain
a section like this:

```
[to-www-infra]
local=/var/lib/repos/infra
remote=www:/var/www/repo/infra
```

## Contact

The *repopush* tool is [developed in a GitLab repository][gitlab].
The author is [Peter Pentchev][roam].

[home]: https://devel.ringlet.net/net/repopush/
[gitlab]: https://gitlab.com/ppentchev/repopush/
[roam]: mailto:roam@ringlet.net
