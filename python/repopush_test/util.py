"""Common functions for the repopush test runner."""

import os
import pathlib
import shlex

from typing import List, Sequence, Union


PathStr = Union[str, os.PathLike]
PathList = List[PathStr]


def cmdstr(cmd: Sequence[PathStr]) -> str:
    """Build a human-readable string for a command."""
    return shlex.join(str(word) for word in cmd)


def expected_steps(src: pathlib.Path) -> int:
    """Determine the number of steps that a command is expected to run for."""
    if src.name.startswith("src-deb-"):
        return 4

    if src.name.startswith("src-yum-"):
        arches = [
            name
            for name in (path.parent.name for path in src.glob("*/repodata") if path.is_dir())
            if name != "SRPMS"
        ]
        return 4 + 2 * len(arches)

    raise NotImplementedError(f"Internal error: expected_steps({src!r})")
